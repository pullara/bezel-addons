<?php

require_once('shortcodes/home-slider.php');
require_once('shortcodes/page-title.php');
require_once('shortcodes/icon-box.php');
require_once('shortcodes/button.php');
require_once('shortcodes/accordion.php');
require_once('shortcodes/tabs.php');
require_once('shortcodes/image-slider.php');
require_once('shortcodes/team-member.php');
require_once('shortcodes/clients.php');
require_once('shortcodes/testimonials.php');
require_once('shortcodes/blog-summary.php');
require_once('shortcodes/portfolio.php');
require_once('shortcodes/modal.php');
require_once('shortcodes/progress-bars.php');
require_once('shortcodes/carousel.php');
require_once('shortcodes/pricing-table.php');
require_once('shortcodes/photo-gallery.php');
require_once('shortcodes/image-box.php');
require_once('shortcodes/menu.php');
require_once('shortcodes/social-icons.php');
require_once('shortcodes/job-offer.php');
require_once('shortcodes/timeline.php');
require_once('shortcodes/common.php');

?>
